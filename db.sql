--- ***** NETTOYAGE DE LA BASE ***** ---
DROP SCHEMA IF EXISTS Articles CASCADE;
DROP SCHEMA IF EXISTS Users CASCADE;
DROP SCHEMA IF EXISTS Committees CASCADE;

--- ****** CREATION DES TABLES ****** ----

--- **** PACKAGE User **** ---
CREATE SCHEMA Users;

    --- ** TABLES ** ---
    CREATE TABLE Users.roles (
        title VARCHAR PRIMARY KEY CHECK (LENGTH(title) > 2)
    );

    INSERT INTO Users.roles (title) VALUES ('admin');
    INSERT INTO Users.roles (title) VALUES ('publisher');
    INSERT INTO Users.roles (title) VALUES ('writer');
    INSERT INTO Users.roles (title) VALUES ('moderator');
    INSERT INTO Users.roles (title) VALUES ('reader');



    CREATE TABLE Users.permissions (
        identifier VARCHAR PRIMARY KEY CHECK (LENGTH(identifier) > 4),
        description TEXT NOT NULL
    );

    INSERT INTO Users.permissions (identifier, description) VALUES ('w_account', 'Create a user');
    INSERT INTO Users.permissions (identifier, description) VALUES ('d_account', 'Delete a user');
    INSERT INTO Users.permissions (identifier, description) VALUES ('u_account_password', 'Update the password of an account');
    INSERT INTO Users.permissions (identifier, description) VALUES ('w_article', 'Write an article');
    INSERT INTO Users.permissions (identifier, description) VALUES ('u_article', 'Edit one of his own article');
    INSERT INTO Users.permissions (identifier, description) VALUES ('d_article', 'Delete one his own article');
    INSERT INTO Users.permissions (identifier, description) VALUES ('submit_article', 'Submit an article to one committee');
    INSERT INTO Users.permissions (identifier, description) VALUES ('r_article_committee', 'Display committee''s articles pending');
    INSERT INTO Users.permissions (identifier, description) VALUES ('u_article_committee', 'Édit one of the article submitted to the Committee');
    INSERT INTO Users.permissions (identifier, description) VALUES ('w_category', 'Create a new category');
    INSERT INTO Users.permissions (identifier, description) VALUES ('u_category', 'Update the category''s title');
    INSERT INTO Users.permissions (identifier, description) VALUES ('w_keyword', 'Create a keyword');
    INSERT INTO Users.permissions (identifier, description) VALUES ('u_keyword', 'Update the keyword''s title');
    INSERT INTO Users.permissions (identifier, description) VALUES ('validate_article', 'Validate an article to be published');
    INSERT INTO Users.permissions (identifier, description) VALUES ('publish_article', 'Publish an article previously validated');
    INSERT INTO Users.permissions (identifier, description) VALUES ('attach_keyword_article', 'Attach a keyword to the article');
    INSERT INTO Users.permissions (identifier, description) VALUES ('w_comment', 'Write a comment');
    INSERT INTO Users.permissions (identifier, description) VALUES ('u_comment', 'Update one of his own comment');
    INSERT INTO Users.permissions (identifier, description) VALUES ('d_comment', 'Delete one of his own comment');
    INSERT INTO Users.permissions (identifier, description) VALUES ('hide_comment_any', 'Hide any comment');
    INSERT INTO Users.permissions (identifier, description) VALUES ('d_comment_any', 'Delete any comments');
    INSERT INTO Users.permissions (identifier, description) VALUES ('highlight_comment', 'Highlight a comment');



    CREATE TABLE Users.role_permission (
        role_title VARCHAR REFERENCES Users.roles(title),
        permission_identifier VARCHAR REFERENCES Users.permissions(identifier),
        PRIMARY KEY (role_title, permission_identifier)
    );

    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('admin', 'w_account');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('admin', 'd_account');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('admin', 'u_account_password');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('writer', 'w_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('writer', 'u_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('writer', 'd_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('writer', 'submit_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'r_article_committee');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'u_article_committee');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'w_category');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'u_category');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'w_keyword');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'u_keyword');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'validate_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'publish_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('publisher', 'attach_keyword_article');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('reader', 'w_comment');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('writer', 'w_comment');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('reader', 'u_comment');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('writer', 'u_comment');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('moderator', 'hide_comment_any');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('moderator', 'd_comment_any');
    INSERT INTO Users.role_permission (role_title, permission_identifier) VALUES ('moderator', 'highlight_comment');



    CREATE TABLE Users.users(
        email VARCHAR PRIMARY KEY CHECK (LENGTH(email) >= 8),
        password VARCHAR NOT NULL,
        firstname VARCHAR,
        lastname VARCHAR,
        nickname VARCHAR UNIQUE NOT NULL,
        mini_bio TEXT ,
        avatar VARCHAR CHECK (SUBSTRING(avatar from 1 for 4) = 'http'),
        role VARCHAR REFERENCES Users.roles(title) NOT NULL,
        created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

        UNIQUE(nickname, firstname, lastname, avatar, mini_bio)
    );

    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('michel.piccoli@gmail.com', '54253771b273275eb6df78674c383883', 'Michel', 'Piccoli', 'Picolo', NULL, 'https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1', 'publisher', '2018-11-03 23:14:32');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('clement.botty@etu.utc.fr', 'e74bf2f3b5efbe1313d8622d0b87bffe', 'Clement', 'Botty', 'Clem.B', 'My dream : Passing NF17 with success !', 'https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1', 'writer', '2018-01-05 10:00:00');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('jean.laporte@utc.fr', '83ce2e291cfed6106f0a3d6752859455', 'Jean', 'Laporte', 'Bg60200', NULL, 'https://robohash.org/utcommodiesse.bmp?size=50x50&set=set1', 'publisher', '2018-11-03 21:14:38');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('eric.dumontet@utc.fr', 'ba91f2f3b5efbe1313d8622d0b87bfa7', 'Eric', 'Dumontet', 'Dj35000', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1', 'publisher', '2018-11-03 23:14:32');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('guy.bedos@outlook.fr', 'ce9e22d04c37caa008a3e8a1326bf8dd', 'Guy', 'Bedos', 'Bushoo', NULL, 'https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1', 'publisher', '2018-11-03 23:14:32');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('pierre.pot@utc.fr', '47852e291cfed6106f0a3d6752859455', 'Pierre', 'Pot', 'PotP', NULL, 'https://robohash.org/eumsitporro.png?size=50x50&set=set1', 'reader', '2019-08-17 07:13:41');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('luc.fau@utc.fr', 'a5b6ce291cfed6106f0a3d6752859455', 'Klarika', 'Setchfield', 'LucF', NULL, 'https://robohash.org/eumsitporro.png?size=50x50&set=set1', 'reader', '2019-09-12 05:13:41');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('ochina@gmail.com', '54253771b273275eb6df78674c383883', 'Cthrine', 'Ceschini', 'Arrowleaf', NULL, 'https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1', 'writer', '2018-11-03 23:14:32');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('cceschini0@bizjournals.com', 'ba91f2f3b5efbe1313d8622d0b87bfa7', 'Cthrine', 'Poolm', 'Cath92', NULL, 'https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1', 'writer', '2018-11-03 23:14:32');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('rtillman1@ftc.gov', 'f818081edc158af73d06280b4493277b', 'Robinet', 'Tillman', 'Saxifrage', NULL, 'https://robohash.org/quisquamevenietperspiciatis.jpg?size=50x50&set=set1', 'publisher', '2020-04-14 23:16:11');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('rgemmill2@epa.gov', '68e3c5c0fc00fb57b9d2213995223665', 'Romonda', 'Gemmill', 'Hairy', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'https://robohash.org/nequeipsamexpedita.jpg?size=50x50&set=set1', 'writer', '2019-02-26 11:45:23');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('ldraaisma3@i2i.jp', '5f5edc8e13c2028b5d6e1c083bd94fa1', 'Layla', 'Draaisma', 'Poisonbulb', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'https://robohash.org/auteosprovident.bmp?size=50x50&set=set1', 'writer', '2019-03-26 18:44:21');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('jsneesby4@gmpg.org', '6a62a05dd02945b798fc006a36f52514', 'Johan', 'Sneesby', 'Margaranthus', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'https://robohash.org/velquaererum.png?size=50x50&set=set1', 'writer', '2019-08-01 23:25:38');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('clomond5@posterous.com', '07d30e4f96377867af520fc2edf4ff8a', 'Conan', 'Lomond', 'Marsh', NULL, 'https://robohash.org/etvoluptasasperiores.jpg?size=50x50&set=set1', 'admin', '2019-04-23 18:25:40');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('fgalliard6@discuz.net', 'b8cc71b7620f1f9ca806caea5e1ee142', 'Florentia', 'Galliard', 'Shootingstar', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 'https://robohash.org/quaevoluptatibusmaxime.png?size=50x50&set=set1', 'moderator', '2019-07-27 02:51:13');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('nsawkin7@dell.com', '298c71765e035c7d910dd1d80f5a26d2', 'Nessi', 'Sawkin', 'Wart', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 'https://robohash.org/ablaudantiumquasi.bmp?size=50x50&set=set1', 'moderator', '2020-06-06 03:34:48');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('aponton8@sciencedaily.com', '5461701ca60f05b9cf24cd9d9d4667c2', 'Arliene', 'Ponton', 'White Avens', NULL, 'https://robohash.org/utexpeditaiste.png?size=50x50&set=set1', 'publisher', '2018-11-20 22:26:44');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('nstriker9@163.com', '93b785a809711b70fee55a75061afb89', 'Nikolaus', 'Striker', 'Germander', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'https://robohash.org/fugavoluptatenam.jpg?size=50x50&set=set1', 'moderator', '2018-12-01 15:04:16');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('sborlanda@eventbrite.com', 'a2b006d7b0b600852ed35524fa0f0c6a', 'Sherri', 'Borland', 'Lecidea', NULL, 'https://robohash.org/quoconsecteturaccusantium.png?size=50x50&set=set1', 'admin', '2020-06-05 08:23:18');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('fszymanekb@slate.com', '9d3cc31a73376c85eb71744cced6acab', 'Ferguson', 'Szymanek', 'Glasswort', NULL, 'https://robohash.org/velitetqui.png?size=50x50&set=set1', 'moderator', '2018-11-18 08:26:37');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('ksetchfieldc@ucla.edu', '83ce2e291cfed6106f0a3d6752859455', 'Klarika', 'Setchfield', 'Brownish', NULL, 'https://robohash.org/eumsitporro.png?size=50x50&set=set1', 'reader', '2019-02-24 03:13:41');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('eballsd@slideshare.net', 'aba1f237d2a3b17fa6eb52bdcd60e6a2', 'Elita', 'Balls', 'Snakewood', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 'https://robohash.org/eumquasunde.bmp?size=50x50&set=set1', 'moderator', '2018-12-14 08:04:19');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('dattenbrowe@cdbaby.com', '8fe712c6d625095240bd3e69fc944928', 'Dalt', 'Attenbrow', 'Hybrid93', NULL, 'https://robohash.org/utcommodiesse.bmp?size=50x50&set=set1', 'moderator', '2019-12-08 20:25:34');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('cloadsmanf@ocn.ne.jp', '63ab7e1d28600f444b1c63cd6ef974f2', 'Carrissa', 'Loadsman', 'Featherleaf', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'https://robohash.org/velnatusunde.bmp?size=50x50&set=set1', 'reader', '2018-12-05 23:20:53');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('bwinearg@hostgator.com', 'aff6b9947358e75e3c02787194dca8af', 'Bart', 'Winear', 'Gladecress', NULL, NULL, 'moderator', '2018-12-30 16:19:40');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('cgatissh@unblog.fr', 'bdc61b5df231ce441bd93e8734c1d57f', 'Carrol', 'Gatiss', 'Knapweed', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'https://robohash.org/sintaddeleniti.jpg?size=50x50&set=set1', 'moderator', '2018-11-18 04:02:01');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('dgwytheri@wunderground.com', 'ba91f2f3b5efbe1313d8622d0b87bfa7', 'Dianne', 'Gwyther', 'California Horkelia', NULL, 'https://robohash.org/consequaturinNULLa.jpg?size=50x50&set=set1', 'moderator', '2020-02-19 16:12:49');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('eganderj@auda.org.au', 'f35b27eae8323bef819ab4e63ad2e5d5', 'Erek', 'Gander', 'Blanketflower', NULL, 'https://robohash.org/quimolestiaedolore.jpg?size=50x50&set=set1', 'reader', '2019-06-03 06:28:55');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('ssouchk@usgs.gov', '649110f06b5538a84f8331ef18a4dcf1', 'Shayne', 'Souch', 'Vetch', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 'https://robohash.org/voluptatemdelectusat.bmp?size=50x50&set=set1', 'writer', '2020-04-30 03:13:11');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('tchafferm@mapy.cz', '9b4bbccdf631354d0ecc26aa873bc6fa', 'Terese', 'Chaffer', 'Janew', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/corporisveritatisvoluptatem.jpg?size=50x50&set=set1', 'moderator', '2019-09-01 00:35:37');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('fhazemann@plala.or.jp', '6739a3dbd16b6ac3fbaf3e7c3bb37701', 'Francklyn', 'Hazeman', 'Eaton87', NULL, 'https://robohash.org/inciduntquirepellat.bmp?size=50x50&set=set1', 'publisher', '2020-05-06 10:32:36');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('hscollando@utexas.edu', 'baa9e0d6a28b2a7f8c55e35050eddd0c', 'Humphrey', 'Scolland', 'Sorrel', NULL, 'https://robohash.org/evenietpariaturtempore.bmp?size=50x50&set=set1', 'publisher', '2019-12-29 09:44:22');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('meakep@mac.com', 'ce9e22d04c37caa008a3e8a1326bf8dd', 'Maximilian', 'Eake', 'Moss', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'https://robohash.org/veritatisenimnam.png?size=50x50&set=set1', 'admin', '2019-10-11 06:25:27');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('hphizackleaq@dagondesign.com', '7aad4c89533a6bb455e212d19b9cb612', 'Helena', 'Phizacklea', 'Goldenbanner', NULL, 'https://robohash.org/officiapossimussuscipit.png?size=50x50&set=set1', 'writer', '2018-11-18 09:40:59');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('abrummittr@bbc.co.uk', '50496e8ac046526352465a61137d82fc', 'Aymer', 'Brummitt', 'Nevada', NULL, 'https://robohash.org/modimagnamadipisci.jpg?size=50x50&set=set1', 'writer', '2019-05-09 02:01:46');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('hstockbridges@ucoz.com', '37f147cc27029309980baa3be2f559ec', 'Heindrick', 'Stockbridge', 'Whitegrass', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 'https://robohash.org/suscipitipsumesse.bmp?size=50x50&set=set1', 'admin', '2020-05-31 16:04:23');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('hskellst@artisteer.com', 'fdef3907742fbc5b23e75a7ba58d9454', 'Hana', 'Skells', 'Swamp_Oak', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 'https://robohash.org/quoadqui.jpg?size=50x50&set=set1', 'reader', '2019-07-25 22:06:52');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('rmcmullenu@constantcontact.com', 'f9fa4f13bd0851109bdfab39d95f5eb2', 'Roshelle', 'McMullen', 'California_Walnut', NULL, 'https://robohash.org/omnissitdoloremque.jpg?size=50x50&set=set1', 'publisher', '2018-12-25 16:40:06');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('amacardlev@chronoengine.com', 'f1a04701a9c42c2d6a3ce19ac303cbf8', 'Agnella', 'MacArdle', 'Cushion_Buckwheat', NULL, NULL, 'admin', '2018-12-10 09:10:56');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('spuntonw@artisteer.com', '96c8fe00ff9f0e5f9b4bf7a454dad6cb', 'Shalna', 'Punton', 'Red_Rodwood', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 'https://robohash.org/ullamnisiqui.jpg?size=50x50&set=set1', 'reader', '2020-04-19 07:33:36');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('dbrumptonx@stumbleupon.com', 'b6f1664728b32a8d7936a9cec7275609', 'Dorian', 'Brumpton', 'Climacium', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 'https://robohash.org/aetconsequuntur.bmp?size=50x50&set=set1', 'reader', '2019-11-05 03:56:45');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('fbeesey@sun.com', 'ae86e48f979cd789c3493308e81073df', 'Fulvia', 'Beese', 'Philodendron', 'Nam ultrices, libero non mattis pulvinar, NULLa pede ullamcorper augue, a suscipit NULLa elit ac NULLa. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', NULL, 'moderator', '2019-02-18 21:19:14');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('cfroggattz@google.fr', '8824caac98bfc4a0cc2fe68199ee88ef', 'Christiane', 'Froggatt', 'Mexican_Pine', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'https://robohash.org/nonanimiomnis.jpg?size=50x50&set=set1', 'writer', '2019-11-16 13:50:11');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('pstillgoe10@quantcast.com', 'f89d59e1851f574b33de1771222c8120', 'Peder', 'Stillgoe', 'Western_White_Spruce', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 'https://robohash.org/maioresearumest.bmp?size=50x50&set=set1', 'admin', '2020-04-08 00:45:12');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('ascahill11@xing.com', '349787ea9a4885bc39e59be435daade4', 'Anselma', 'Scahill', 'Rosemary', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 'https://robohash.org/velinciduntconsequatur.bmp?size=50x50&set=set1', 'writer', '2019-03-14 11:09:09');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('mblunderfield12@tamu.edu', 'c208f928bdb2a048f9dc1b8a6cf9df53', 'Monro', 'Blunderfield', 'Columbine', NULL, 'https://robohash.org/remnostrumaliquam.bmp?size=50x50&set=set1', 'admin', '2020-04-14 20:39:10');
    INSERT INTO Users.users (email, password, firstname, lastname, nickname, mini_bio, avatar, role, created_at) VALUES ('tabadam13@pen.io', '789900fef3f6cc705de829b0f40c7544', 'Trista', 'Abadam', 'Alaska74', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/ainciduntofficiis.jpg?size=50x50&set=set1', 'publisher', '2019-04-24 03:32:40');






--- **** PACKAGE Committee **** ---
CREATE SCHEMA Committees;

    --- ** TYPES ** ---
    CREATE TYPE Committees.STATUS_APPROVAL AS ENUM ('pending', 'approved', 'refused');

    --- ** TABLES ** ---
    CREATE TABLE Committees.editorial_committees(
      name VARCHAR PRIMARY KEY,
      description TEXT NOT NULL,
      created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
      created_by VARCHAR REFERENCES Users.users(email) NOT NULL
    );

    INSERT INTO Committees.editorial_committees (name, description, created_by) VALUES ('TopGear', 'We are in charge of every motorsport articles', 'rtillman1@ftc.gov');
    INSERT INTO Committees.editorial_committees (name, description, created_by) VALUES ('KitchenChief', 'We are in charge of cooking articles', 'aponton8@sciencedaily.com');
    INSERT INTO Committees.editorial_committees (name, description, created_by) VALUES ('Uvs', 'We are looking for news talking about UTC Uvs', 'jean.laporte@utc.fr');
    INSERT INTO Committees.editorial_committees (name, description, created_by) VALUES ('MrWeather', 'We publish weather news every morning', 'tabadam13@pen.io');
    INSERT INTO Committees.editorial_committees (name, description, created_by) VALUES ('Eureka', 'We love publishing scientiq articles', 'rmcmullenu@constantcontact.com');
    INSERT INTO Committees.editorial_committees (name, description, created_by) VALUES ('BodyBuilders', 'We treat every fitness articles', 'hscollando@utexas.edu');



    CREATE TABLE Committees.committee_user(
        user_email VARCHAR REFERENCES Users.users(email),
        committee_name VARCHAR REFERENCES Committees.editorial_committees(name),
        status Committees.STATUS_APPROVAL NOT NULL,
        requested_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
        decided_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP CHECK (date(decided_at) > date(requested_at)),

        PRIMARY KEY(user_email, committee_name)
    );

    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('michel.piccoli@gmail.com', 'TopGear', 'approved', '2020-10-04 14:20:17', '2020-12-07 18:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('guy.bedos@outlook.fr', 'Eureka', 'refused', '2020-12-04 10:20:17', '2020-12-07 08:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('jean.laporte@utc.fr', 'Uvs', 'approved', '2020-12-04 10:20:17', '2020-12-07 11:33:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('eric.dumontet@utc.fr', 'BodyBuilders', 'pending', '2020-08-03 19:20:17', '2020-12-07 10:39:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('rtillman1@ftc.gov', 'TopGear', 'approved', '2020-08-03 14:20:17', '2020-12-07 08:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('aponton8@sciencedaily.com', 'TopGear', 'pending', '2020-05-21 09:22:45', NULL);
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('hscollando@utexas.edu', 'TopGear', 'pending', '2020-05-21 09:22:45', NULL);
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('aponton8@sciencedaily.com', 'KitchenChief', 'approved', '2020-08-03 14:20:17', '2020-11-23 17:29:33');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('fhazemann@plala.or.jp', 'KitchenChief', 'pending', '2020-05-21 09:22:45', NULL);
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('fhazemann@plala.or.jp', 'BodyBuilders', 'refused', '2020-04-23 18:00:21', '2020-12-07 08:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('rmcmullenu@constantcontact.com', 'KitchenChief', 'pending', '2020-05-21 09:22:45', NULL);
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('rtillman1@ftc.gov', 'Uvs', 'pending', '2020-04-23 18:00:21', NULL);
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('fhazemann@plala.or.jp', 'Uvs', 'approved', '2020-05-21 09:22:45', '2020-07-13 13:33:33');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('tabadam13@pen.io', 'MrWeather', 'approved', '2020-08-03 14:20:17', '2020-12-07 08:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('hscollando@utexas.edu', 'MrWeather', 'pending', '2020-05-21 09:22:45', NULL);
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('rmcmullenu@constantcontact.com', 'Eureka', 'approved', '2020-04-23 18:00:21', '2020-12-07 08:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('fhazemann@plala.or.jp', 'Eureka', 'refused', '2020-04-23 18:00:21', '2020-12-07 08:36:22');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('tabadam13@pen.io', 'Eureka', 'approved', '2020-08-03 14:20:17', '2020-11-23 17:29:33');
    INSERT INTO Committees.committee_user (user_email, committee_name, status, requested_at, decided_at) VALUES ('hscollando@utexas.edu', 'BodyBuilders', 'approved', '2020-04-23 18:00:21', '2020-11-23 17:29:33');



--- **** PACKAGE Article **** ---
CREATE SCHEMA Articles;

    --- ** TYPES ** ---
    CREATE TYPE Articles.ARTICLE_EVT AS ENUM ('created', 'edited', 'submited', 'validated', 'reported', 'rejected', 'published', 'deleted');
    CREATE TYPE Articles.COMMENT_EVT AS ENUM ('created', 'highlighted', 'hidden', 'deleted');

    --- ** TABLES ** ---
    CREATE TABLE Articles.articles (
        slug VARCHAR PRIMARY KEY,
        title VARCHAR NOT NULL,
        showcase_url VARCHAR NOT NULL CHECK (SUBSTRING(showcase_url from 1 for 4) = 'http'),
        honnored_at TIMESTAMP WITH TIME ZONE,
        honnored_until TIMESTAMP WITH TIME ZONE CHECK (date(honnored_until) > date(honnored_at)) ,
        honnored_by VARCHAR REFERENCES Users.users(email),

        CONSTRAINT CheckHonnored CHECK(
            (honnored_at IS NOT NULL AND honnored_until IS NOT NULL AND honnored_by IS NOT NULL) OR 
            (honnored_at IS NULL AND honnored_until IS NULL AND honnored_by IS NULL)
        )
    );

    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('how-to-validate-nf17', 'How to validate NF17', 'https://robohash.org/saepeeiusveritatis.bmp?size=50x50&set=set1', NULL, NULL, NULL);
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('a-new-electric-peugeot', 'A new Electric Peugeot !', 'https://robohash.org/saepeeiusveritatis.bmp?size=50x50&set=set1', '2020-05-30 10:36:22', '2020-06-15 10:00:00', 'michel.piccoli@gmail.com');
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('will-renault-survive', 'Will Renault survive ?', 'https://robohash.org/quisquamoptioaut.jpg?size=50x50&set=set1', NULL, NULL, NULL);
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('today-it-s-pizza', 'Today it''s pizza !', 'https://robohash.org/enimaliassit.jpg?size=50x50&set=set1', NULL, NULL, NULL);
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('how-about-vegan-cheese', 'How about vegan cheese ?', 'https://robohash.org/saepeeiusveritatis.bmp?size=50x50&set=set1', '2020-07-21 11:31:22', '2020-08-20 11:36:22', 'aponton8@sciencedaily.com');
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('weather-2020-05-29', 'Weather 29 May 2020', 'https://robohash.org/saepeeiusveritatis.bmp?size=50x50&set=set1', NULL, NULL, NULL);
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('weather-2020-05-30', 'Weather 30 May 2020', 'https://robohash.org/voluptatemexercitationemnobis.bmp?size=50x50&set=set1', NULL, NULL, NULL);
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('inside-black-holes', 'Inside Black Holes', 'https://dummyimage.com/198x148.png/ff4444/ffffff', '2020-06-02 22:00:00', '2020-06-25 22:00:00','rmcmullenu@constantcontact.com');
    INSERT INTO Articles.articles (slug, title, showcase_url, honnored_at, honnored_until, honnored_by) VALUES ('a-new-street-workout-park', 'A new street workout park !', 'https://dummyimage.com/890x113.bmp/4d1abf/ffffff', NULL, NULL, NULL);


    CREATE TABLE Articles.blocks (
        id SERIAL PRIMARY KEY,
        article_slug VARCHAR REFERENCES Articles.articles(slug) NOT NULL,
        title VARCHAR NOT NULL,
        content TEXT,
        image_url VARCHAR CHECK (SUBSTRING(image_url from 1 for 4) = 'http'),
        pos INT CHECK (pos > 0) NOT NULL,

        CONSTRAINT CheckContent CHECK(
            (content IS NULL AND image_url IS NOT NULL) OR 
            (content IS NOT NULL AND image_url IS NULL)
        ),
        UNIQUE(article_slug, title)
    );

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('how-to-validate-nf17', 'One of the most interesting UVs this semester', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', NULL, 1);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('how-to-validate-nf17', 'I spent so much time working on it !', 'About 10 hours since the launch of the projet one weak ago', NULL, 2);

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('inside-black-holes', 'A whole new perspective for Cosmology ', 'After tracking it for five nights, scientists have revealed to the world the image of a supermassive black hole in the heart of the M87 Galaxy. A picture obtained by the international collaboration Event Horizon Telescope. A historical moment.', NULL, 1);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('inside-black-holes', 'This is the First-Ever Image of a Black Hole', NULL, 'https://news.cnrs.fr/sites/default/files/styles/visuel_principal/public/assets/images/blackholeshadowm87april112017creditstheehtcollaboration.jpg', 2);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('inside-black-holes', 'Where nothing escapes ', 'This is the IRAM 30-metre telescope that helped to generate the very first image of a black hole, proudly announces Frédéric Gueth, CNRS researcher and Deputy Director of the IRAM', NULL, 3);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('inside-black-holes', 'Confirming general relativity', 'It is the existence of this event horizon surrounding a black hole that is evidenced by the image released by the EHT collaboration.', NULL, 4);

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('a-new-electric-peugeot', 'A little car full of energy', 'Peugeot , earlier this year introduced its new Peugeot 208. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id NULLa ultrices aliquet.', NULL, 1);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('a-new-electric-peugeot', 'New Peugeot 208', NULL, 'https://www.caroom.fr/uploads/models/peugeot-208.png', 2);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('a-new-electric-peugeot', 'Let''s talk about autonomy', 'It is a desaster : who wants to wait 40 minutes in an electric station while you can make 1000 kilometers with the petrol one. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id NULLa ultrices aliquet.', NULL, 3);

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('will-renault-survive', 'A massive loan from the State', 'As you maybe heard, Renault is about to receive a massive Loan from the French governement. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id NULLa ultrices aliquet.', NULL, 1);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('will-renault-survive', 'Will it be sufficient ? ', 'If we look at the stock exchange, the Value has took 17% on the 28th of May. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id NULLa ultrices aliquet.', NULL, 2);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('will-renault-survive', 'Stock market', NULL, 'https://www.helgilibrary.com/charts/image/135424/0/', 3);

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('how-about-vegan-cheese', '70 Delicious plant based cheeses', NULL, 'https://images-na.ssl-images-amazon.com/images/I/81rp3oNgz-L.jpg', 1);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('how-about-vegan-cheese', 'I promise it'' tasty', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', NULL, 2);

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('weather-2020-05-29', 'A sunny day !', 'At 4 pm the temperature will be around 28°C. Phasellus in felis. Donec semper sapien a libero. Nam dui.', NULL, 1);
    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('weather-2020-05-30', 'Some clouds in the sky', 'Compicity will be a bit cloudy this morning. . Phasellus in felis. Donec semper sapien a libero. Nam dui.', NULL, 1);

    INSERT INTO Articles.blocks (article_slug, title, content, image_url, pos) VALUES ('a-new-street-workout-park', 'UTC Student are glad to see that', 'Street workout is a physical activity performed mostly in outdoor parks or public facilities. It became a popular movement in Russia, Eastern Europe, and the United States, especially in New York City, Baltimore''s urban neighborhoods, Myanmar, and Morocco.', NULL, 1);


    CREATE TABLE Articles.article_events (
        id SERIAL PRIMARY KEY,
        article_slug VARCHAR REFERENCES Articles.articles(slug) NOT NULL,
        user_email VARCHAR REFERENCES Users.users(email) NOT NULL,
        event Articles.ARTICLE_EVT NOT NULL,
        justification TEXT,
        occured_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
    );

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-to-validate-nf17', 'ochina@gmail.com', 'created', NULL, '2020-01-01 13:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-to-validate-nf17', 'ochina@gmail.com', 'edited', NULL, '2020-01-03 16:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-to-validate-nf17', 'ochina@gmail.com', 'edited', NULL, '2020-01-07 18:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-to-validate-nf17', 'ochina@gmail.com', 'submited', 'Please find an interesting article about the NF17 UV', '2020-01-07 19:30:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-to-validate-nf17', 'jean.laporte@utc.fr', 'validated', 'Very well written !', '2020-01-08 12:30:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-to-validate-nf17', 'jean.laporte@utc.fr', 'published', NULL, '2020-01-08 13:30:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'abrummittr@bbc.co.uk', 'created', NULL, '2020-02-01 13:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'abrummittr@bbc.co.uk', 'edited', NULL, '2020-02-01 15:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'abrummittr@bbc.co.uk', 'submited', 'It''s a fantastic news', '2020-02-01 15:30:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'tabadam13@pen.io', 'reported', 'Too many orthographic mistakes', '2020-02-01 19:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'abrummittr@bbc.co.uk', 'edited', NULL, '2020-02-02 11:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'abrummittr@bbc.co.uk', 'submited', 'Please find the corrected version of my article', '2020-02-02 11:30:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'tabadam13@pen.io', 'validated', 'Indeed, that''s much better !', '2020-02-03 10:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('inside-black-holes', 'tabadam13@pen.io', 'published', NULL, '2020-02-03 10:20:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-electric-peugeot', 'clement.botty@etu.utc.fr', 'created', NULL, '2020-03-01 10:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-electric-peugeot', 'clement.botty@etu.utc.fr', 'submited', NULL, '2020-03-01 10:40:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-electric-peugeot', 'michel.piccoli@gmail.com', 'validated', 'I knew you were a great writer ;)', '2020-03-01 11:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-electric-peugeot', 'michel.piccoli@gmail.com', 'published', NULL, '2020-03-01 12:00:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('will-renault-survive', 'clement.botty@etu.utc.fr', 'created', NULL, '2020-03-02 17:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('will-renault-survive', 'clement.botty@etu.utc.fr', 'submited', 'Here is an other article for Renault', '2020-03-02 18:40:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('will-renault-survive', 'michel.piccoli@gmail.com', 'rejected', 'You''ve already write for peugeot two hours ago ...', '2020-03-02 19:00:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('today-it-s-pizza', 'ochina@gmail.com', 'created', NULL, '2020-03-02 15:00:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-about-vegan-cheese', 'rgemmill2@epa.gov', 'created', NULL, '2020-04-02 08:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-about-vegan-cheese', 'rgemmill2@epa.gov', 'submited', NULL, '2020-04-02 09:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-about-vegan-cheese', 'aponton8@sciencedaily.com', 'validated', NULL, '2020-04-03 10:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-about-vegan-cheese', 'aponton8@sciencedaily.com', 'published', NULL, '2020-04-03 10:05:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('how-about-vegan-cheese', 'aponton8@sciencedaily.com', 'deleted', 'There was apparently fake news in the article', '2020-04-07 17:00:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('weather-2020-05-29', 'ascahill11@xing.com', 'created', NULL, '2020-05-28 17:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('weather-2020-05-29', 'ascahill11@xing.com', 'submited', NULL, '2020-05-28 18:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('weather-2020-05-29', 'tabadam13@pen.io', 'validated', 'Good news for tommorow then !', '2020-05-28 19:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('weather-2020-05-29', 'tabadam13@pen.io', 'published', NULL, '2020-05-28 20:00:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('weather-2020-05-30', 'ascahill11@xing.com', 'created', NULL, '2020-05-29 18:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('weather-2020-05-29', 'ascahill11@xing.com', 'submited', NULL, '2020-05-29 18:30:00');

    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'cfroggattz@google.fr', 'created', NULL, '2020-06-01 18:00:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'cfroggattz@google.fr', 'edited', NULL, '2020-06-01 18:20:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'cfroggattz@google.fr', 'edited', NULL, '2020-06-01 18:40:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'cfroggattz@google.fr', 'edited', NULL, '2020-06-01 19:20:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'cfroggattz@google.fr', 'submited', 'I wrote this article for the new street workout park !', '2020-06-01 19:30:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'hscollando@utexas.edu', 'validated', 'Well written, thank you cfrog !', '2020-06-02 15:20:00');
    INSERT INTO Articles.article_events (article_slug, user_email, event, justification, occured_at) VALUES ('a-new-street-workout-park', 'hscollando@utexas.edu', 'published', NULL, '2020-06-02 15:30:00');



    CREATE TABLE Articles.comments (
        id SERIAL PRIMARY KEY,
        article_slug VARCHAR REFERENCES Articles.articles(slug) NOT NULL,
        title VARCHAR NOT NULL,
        content TEXT NOT NULL,
        rating INT CHECK (rating >= 0),
        ref_comment INT REFERENCES Articles.comments(id)
    );

    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('how-to-validate-nf17', 'Thanks you !', 'For all the tips you give to us in order to validate', 5, NULL );
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('how-to-validate-nf17', 'Good article', 'You should have given us more details', 3,     NULL );
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('how-to-validate-nf17', 'You''re welcome', 'It has been a pleasure to read your comment, thanks !', NULL, 1);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('how-to-validate-nf17', 'I will never pass', 'Nice article, but I''m sure I will never pass', 4, NULL);
    
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('inside-black-holes', 'Very interesting', 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?', 5, NULL);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('inside-black-holes', 'Don''t you have a bibiolgraphy ? ', 'Corporis suscipit laboriosam, nisi ut aliquid ex ea commodi.', 2, NULL);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('inside-black-holes', 'No I don''t ', 'Sorry', NULL, 6);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('inside-black-holes', 'That''s not very professionnal', 'Corporis suscipit laboriosam, nisi ut aliquid ex ea commodi.', NULL, 7);

    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('a-new-electric-peugeot', 'No one like electric cars', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 2, NULL);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('a-new-electric-peugeot', 'I so much love electric cars !', 'Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.', 5, NULL);

    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('how-about-vegan-cheese', 'Everything you wrote is Fake news !', 'Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.', 0, NULL);

    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('weather-2020-05-29', 'Nice to see the sun', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment', 4, NULL);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('weather-2020-05-29', 'Glad to read your comment !', ' every pleasure is to be welcomed and every pain avoided', NULL, 12);

    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('a-new-street-workout-park', 'My muscles will explode !', 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?', 5, NULL);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('a-new-street-workout-park', 'Will it be open every day ? ', 'Nisi ut aliquid ex ea commodi', 4, NULL);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('a-new-street-workout-park', 'Yes', 'Open from Monday to Sunday !', NULL, 15);
    INSERT INTO Articles.comments (article_slug, title, content, rating, ref_comment) VALUES ('a-new-street-workout-park', 'Cool article', 'Voluptate velit esse quam nihil molestiae', 5, NULL);



    CREATE TABLE Articles.comment_events (
        id SERIAL PRIMARY KEY,
        comment_id INT NOT NULL,
        user_email VARCHAR REFERENCES Users.users(email) NOT NULL,
        event Articles.COMMENT_EVT NOT NULL,
        occured_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
    );

    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (1, 'pierre.pot@utc.fr', 'created', '2020-08-01 10:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (2, 'luc.fau@utc.fr', 'created', '2020-08-02 11:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (3, 'ochina@gmail.com', 'created', '2020-08-03 12:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (4, 'luc.fau@utc.fr', 'created', '2020-08-04 13:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (5, 'ksetchfieldc@ucla.edu', 'created', '2020-08-05 14:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (6, 'pierre.pot@utc.fr', 'created', '2020-08-06 15:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (7, 'abrummittr@bbc.co.uk', 'created', '2020-08-07 16:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (8, 'pierre.pot@utc.fr', 'created', '2020-08-08 17:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (9, 'eganderj@auda.org.au', 'created', '2020-08-09 18:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (10, 'hskellst@artisteer.com', 'created', '2020-08-10 19:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (11, 'cloadsmanf@ocn.ne.jp', 'created', '2020-08-11 20:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (12, 'luc.fau@utc.fr', 'created', '2020-08-12 21:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (13, 'ascahill11@xing.com', 'created', '2020-08-13 22:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (14, 'pierre.pot@utc.fr', 'created', '2020-08-14 23:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (15, 'dbrumptonx@stumbleupon.com', 'created', '2020-08-15 09:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (16, 'cfroggattz@google.fr', 'created', '2020-08-16 10:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (17, 'pierre.pot@utc.fr', 'created', '2020-08-17 11:00:00');

    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (15, 'fbeesey@sun.com', 'highlighted', '2020-08-15 17:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (3, 'dattenbrowe@cdbaby.com', 'highlighted', '2020-09-22 15:00:00');

    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (7, 'tchafferm@mapy.cz', 'hidden', '2020-07-12 13:00:00');
    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (8, 'fbeesey@sun.com', 'hidden', '2020-07-12 13:30:00');

    INSERT INTO Articles.comment_events (comment_id, user_email, event, occured_at) VALUES (11, 'dattenbrowe@cdbaby.com', 'deleted', '2020-09-27 19:00:00');



    CREATE TABLE Articles.keywords (
        title VARCHAR PRIMARY KEY,
        created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
        created_by VARCHAR REFERENCES Users.users(email) NOT NUll 
    );

    INSERT INTO Articles.keywords (title, created_by) VALUES ('database', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('uv', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('ai23', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('nf17', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('utc', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('db', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('sql', 'jean.laporte@utc.fr');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('exam', 'jean.laporte@utc.fr');

    INSERT INTO Articles.keywords (title, created_by) VALUES ('cosmology', 'tabadam13@pen.io');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('black holes', 'tabadam13@pen.io');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('science', 'tabadam13@pen.io');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('space', 'tabadam13@pen.io');

    INSERT INTO Articles.keywords (title, created_by) VALUES ('car', 'michel.piccoli@gmail.com');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('electric', 'michel.piccoli@gmail.com');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('208', 'michel.piccoli@gmail.com');

    INSERT INTO Articles.keywords (title, created_by) VALUES ('vegan', 'aponton8@sciencedaily.com');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('cheese', 'aponton8@sciencedaily.com');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('recipe', 'aponton8@sciencedaily.com');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('veggi', 'aponton8@sciencedaily.com');

    INSERT INTO Articles.keywords (title, created_by) VALUES ('weather', 'tabadam13@pen.io');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('forecast', 'tabadam13@pen.io');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('sun', 'tabadam13@pen.io');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('compiegne', 'tabadam13@pen.io');
    
    INSERT INTO Articles.keywords (title, created_by) VALUES ('muscles', 'hscollando@utexas.edu');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('street workout', 'hscollando@utexas.edu');
    INSERT INTO Articles.keywords (title, created_by) VALUES ('workout parks', 'hscollando@utexas.edu');


    CREATE TABLE Articles.article_keyword (
        article_slug VARCHAR REFERENCES Articles.articles(slug),
        keyword_title VARCHAR REFERENCES Articles.keywords(title),
        PRIMARY KEY (article_slug, keyword_title)
    );

    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'database');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'uv');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'ai23');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'nf17');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'utc');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'db');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'sql');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'exam');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-to-validate-nf17', 'science');


    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('inside-black-holes', 'science');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('inside-black-holes', 'cosmology');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('inside-black-holes', 'black holes');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('inside-black-holes', 'space');
    
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-about-vegan-cheese', 'vegan');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-about-vegan-cheese', 'cheese');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-about-vegan-cheese', 'recipe');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('how-about-vegan-cheese', 'veggi');

    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('weather-2020-05-29', 'weather');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('weather-2020-05-29', 'forecast');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('weather-2020-05-29', 'sun');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('weather-2020-05-29', 'compiegne');

    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('a-new-street-workout-park', 'compiegne');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('a-new-street-workout-park', 'muscles');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('a-new-street-workout-park', 'street workout');
    INSERT INTO Articles.article_keyword (article_slug, keyword_title) VALUES ('a-new-street-workout-park', 'workout parks');



    CREATE TABLE Articles.categories (
        title VARCHAR PRIMARY KEY,
        created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
        created_by VARCHAR REFERENCES Users.users(email) NOT NUll,
        parent_category VARCHAR REFERENCES Articles.categories(title),
        committee VARCHAR REFERENCES Committees.editorial_committees(name) NOT NULL
    );

    --- Réprésentation de la hierarchie des catégories
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Database', 'jean.laporte@utc.fr', NULL, 'Uvs');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('SQL', 'jean.laporte@utc.fr', 'Database', 'Uvs');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('MySql', 'jean.laporte@utc.fr', 'SQL', 'Uvs');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Postgre SQL', 'jean.laporte@utc.fr', 'SQL', 'Uvs');
--4
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('NoSQL', 'jean.laporte@utc.fr', 'Database', 'Uvs');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('MongoDB', 'jean.laporte@utc.fr', 'NoSQL', 'Uvs');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Firebase', 'jean.laporte@utc.fr', 'NoSQL', 'Uvs');
--7
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Normalisation', 'jean.laporte@utc.fr', 'Database', 'Uvs');
--8
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('UV', 'jean.laporte@utc.fr', NULL, 'Uvs');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('P20', 'jean.laporte@utc.fr', 'UV', 'Uvs');

    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Motorsport', 'rtillman1@ftc.gov', NULL, 'TopGear');

    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Car', 'rtillman1@ftc.gov', NULL, 'TopGear');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Electric car', 'rtillman1@ftc.gov', 'Car', 'TopGear');
--13
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Motorcycle', 'rtillman1@ftc.gov', NULL, 'TopGear');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Electric Motorcyle', 'rtillman1@ftc.gov', 'Motorcycle', 'TopGear');

    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Vegan', 'aponton8@sciencedaily.com', NULL, 'KitchenChief');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Vegetables', 'aponton8@sciencedaily.com', NULL, 'KitchenChief');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Fruits', 'aponton8@sciencedaily.com', NULL, 'KitchenChief');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Recipe', 'aponton8@sciencedaily.com', NULL, 'KitchenChief');
--19
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Weather', 'tabadam13@pen.io', NULL, 'MrWeather');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Forecast', 'tabadam13@pen.io', 'Weather', 'MrWeather');

    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Science', 'rmcmullenu@constantcontact.com', NULL, 'Eureka');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Astronomy', 'rmcmullenu@constantcontact.com', 'Science', 'Eureka');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Cosmology', 'rmcmullenu@constantcontact.com', 'Astronomy', 'Eureka');
--24
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Fitness', 'hscollando@utexas.edu', NULL, 'BodyBuilders');
    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Weight Room', 'hscollando@utexas.edu', 'Fitness', 'BodyBuilders');

    INSERT INTO Articles.categories (title, created_by, parent_category, committee) VALUES ('Street Workout', 'hscollando@utexas.edu', NULL, 'BodyBuilders');



    CREATE TABLE Articles.article_category (
        article_slug VARCHAR REFERENCES Articles.articles(slug),
        category_title VARCHAR REFERENCES Articles.categories(title),
        PRIMARY KEY(article_slug, category_title)
    );

    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('how-to-validate-nf17', 'Database');
    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('how-to-validate-nf17', 'Postgre SQL');

    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('inside-black-holes', 'Cosmology');

    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('how-about-vegan-cheese', 'Vegan');
    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('how-about-vegan-cheese', 'Recipe');

    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('weather-2020-05-29', 'Forecast');

    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('a-new-street-workout-park', 'Fitness');
    INSERT INTO Articles.article_category (article_slug, category_title) VALUES ('a-new-street-workout-park', 'Street Workout');