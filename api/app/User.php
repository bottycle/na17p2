<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users.users';
    protected $primaryKey = 'email';
    protected $keyType = 'string';

    protected $hidden = ['password'];

    
    public function committees()
    {
        return $this->belongsToMany(Committee::class, 'committees.committee_user', 'user_email', 'committee_name');
    }

}
