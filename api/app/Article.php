<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles.articles';
    protected $primaryKey = 'slug';
    protected $keyType = 'string';
    protected $fillable = ['slug', 'title', 'showcase_url'];
    public $timestamps = false;

    public function blocks()
    {
        return $this->hasMany(Block::class, 'article_slug');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'articles.article_category', 'article_slug', 'category_title');
    }

    public function events()
    {
        return $this->hasMany(ArticleEvent::class, 'article_slug');
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class, 'articles.article_keyword', 'article_slug', 'keyword_title');
    }
}



