<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;

class UserController extends Controller
{
    public function index()
    {
        return User::with('committees')->get();
    }

    public function get(User $user)
    {
        $user->committees;
        return $user;
    }

    public function update(User $user)
    {

        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->nickname = request('nickname');
        $user->mini_bio = request('mini_bio');
        $user->avatar = request('avatar');


        $user->save();

        return ["code" => 200, "response" => "User updated with success thanks to this AI23 Laravel API!"];
    }

    public function destroy(User $user)
    {
        $user->delete();

        $l = $user->email." has been successfully deleted. ";
        return ["code" => 200, "response" => $l];
    }
}
