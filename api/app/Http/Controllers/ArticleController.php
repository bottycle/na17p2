<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        return ["articles" => Article::with(['blocks', 'events.user', 'categories', 'keywords'])->get()];
    }

    public function get(Article $article) // Ici l'article est automatiquement récupéré en passant le slug dans l'URL de la route
    {
        // On demande explicitement à ORM d'instancier dynamiquement les blocs et les catégories de l'article en question
        $article->blocks;
        $article->categories;

        return ["article" => $article];
    }

    
    public function store(Request $request)
    {
        
        Article::create($request->validate([
            'slug' => 'required',
            'title' => 'required|min:3|max:255',
            'showcase_url' => 'required'
        ]));

        return ["code" => 201, "response" => "Article created with success thanks to this AI23 Laravel API!"];
    }

    public function update(Article $article)
    {
        request()->validate([
            'title' => 'required|min:3|max:255',
            'slug' => 'required',
            'showcase_url' => 'required'
        ]);

        $article->title = request('title');
        $article->slug = request('slug');
        $article->showcase_url = request('showcase_url');

        $article->save();

        return ["code" => 200, "response" => "Article updated with success thanks to this AI23 Laravel API!"];
    }

    public function destroy(Article $article)
    {
        $article->delete();
        // Il faudrait supprimer tous les blocks liés à l'article aussi ... 
        $l = $article->slug." has been successfully deleted. ";
        return ["code" => 200, "response" => $l];
    }


    
}
