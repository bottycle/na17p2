<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'articles.categories';

    protected $primaryKey = 'title';
    protected $keyType = 'string';

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'articles.article_category', 'category_title', 'article_slug');
    }
}
