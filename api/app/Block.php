<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'articles.blocks';

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
