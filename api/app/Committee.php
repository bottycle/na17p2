<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model
{
    protected $table = 'committees.editorial_committees';
    protected $primaryKey = 'name';
    protected $keyType = 'string';

    public function users()
    {
        return $this->belongsToMany(User::class, 'committees.committee_user', 'committee_name', 'user_email');
    }
}
