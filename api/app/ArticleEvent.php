<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleEvent extends Model
{
    protected $table = 'articles.article_events';

    public function user()
    {
        return $this->hasOne(User::class, 'email', 'user_email');
    }
}
