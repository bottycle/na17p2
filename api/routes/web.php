<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('articles', 'ArticleController@index');
Route::get('articles/{article}', 'ArticleController@get');
Route::post('articles', 'ArticleController@store');
Route::put('articles/{article}', 'ArticleController@update');
Route::delete('articles/{article}', 'ArticleController@destroy');


Route::get('users', 'UserController@index');
Route::get('users/{user}', 'UserController@get');
Route::post('users','UserController@store');
Route::delete('users/{user}', 'UserController@destroy');
