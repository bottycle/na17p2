# Note de Clarification

## Objets

   #### Article
   - Contient des blocs

   - Possède : 
      - Un titre
      - Une image vitrine (Hypothèse : elle apparait lorsqu'on partage l'article sur les réseaux sociaux)
      - Un statut *(en rédaction, soumis, en relecture, rejeté, à réviser, validé)*
      - Un slug (identifiant de l'article dans l'url)
      - Un ou plusieurs mots clefs
      - Une ou plusieurs rubriques / sous rubriques
      - Un numéro de version calculé (Ex : Avant d'être soumis 0.x, édité après avoir été soumis : 1.x, etc. )

   - Peut être :
      - lié à plusieurs autres articles
      - mis à l'honneur par un rédacteur jusqu'a une certaine date (Hypothèse)
      - supprimé par son rédacteur
      ---> Possède une date de suppression

   - Est : 
      - écrit par un rédacteur
      ---> Possède une date de création de l'article

      - validé par un éditeur 
      ---> Possède une date de validation

      - publié par un éditeur
      ---> Possède une date de publication 



   #### Bloc
   - Possède :  
      - un titre
      - du texte **ou** une image
      - un position dans l'article (Ex : Pour un article à 5 bloc, le bloc en position 1 est en premier, le 3 au milieu et le 5 en dernier) (Hypothèse)
   
   - Concerne un article


   #### Rubrique
   - Est crée par un Éditeur  
   - Possède un libellé
   - Concerne plusieurs articles
   - Peut être lié à une rubrique mère **ou** être une rubrique "racine" 


   #### Mots clefs
   - Est créé par un Éditeur
   - Possède un libellé
   - Concerne plusieurs articles


   #### Commentaire
   - Est créé par un Lecteur

   - Possède : 
      - un titre
      - un texte
      - une date de publication (Hypothèse)
      - une note

   - Peut être : 
      - supprimé par son auteur **ou** par un modérateur
      ---> Possède une date de suppression
   
      - masqué par un moderateur
      ---> Possède une date de masquage

      - mis en exergue par un modérateur
      ---> Possède une date de mise en exergue


   #### Commité Editorial
   - Est : 
      - spécialisé dans une ou plusieurs catégorie
      - créé par un utilisateur
      ---> Possède une date de création

   - Possède : 
      - un nom de commité (Ex : Les pilotes chevronnés -> Catégorie Automobile / Moto)
      - une description (Ex : En charge des articles portant sur le sport automobile, nous organisons des rassemblements de 4L à nos heures perdues.)

   - Concerne plusieurs éditeurs
      ---> Un éditeur à alors, vis à vis du commité, le statut "En attente d'adhésion", "accepté" ou "refusé"
      ---> Une date de demande d'adhésion est générée
      ---> Une date de décision est générée


   #### Utilisateur
   - Possède : 
      - une adresse email (Hypothèse)
      - un mot de passe (Hypothèse)
      - un nom (Hypothèse)
      - un prénom (Hypothèse)
      - une mini biographie pour se décrire aux autres membres (Hypothèse)
      - une photo de profil (Hypothèse) 
      - un rôle (Administrateur, Rédacteur, Éditeur, Modérateur)


   #### Rôle
   - Possède : 
      - un intitulé (Administrateur, Rédacteur, Éditeur, Modérateur)
      - des permissions (Hypothèse)

   - Concerne plusieurs utilisateurs


   #### Permission
   - Possède un intitulé (Ex : r_articles : Permission de lecture de tous les articles, w_article : Permission d'écriture d'un article)
   - Possède une description (Ex : Afficher l'ensemble des articles)


   

## Utilisateurs

1. Administrateurs 
2. Rédacteurs
3. Editeurs
4. Modérateurs
5. Lecteurs 

## Fonctions

   #### Administrateur
   - Créer des comptes
   - Supprimer des comptes

   #### Rédacteur
   - Créer un article
   - Enregister le brouillon d'un article (Hypothèse) (Statut "edited" avant d'être "submited" )
   - Voir **ses** articles en cours de rédaction
   - Supprimer un de **ses** articles
   - Récupérer un de **ses** articles supprimé
   - Soumettre un article au comité éditorial

   #### Editeur
   - Voir l'ensemble des articles en affichant leur date de rédaction, leur statut, le rédacteur, les rubriques / sous rubriques et les mots clefs associés (Hypothèse)

   - Filtrer ces articles : 
      - Par le statut
      - Par le rédacteur
      - Par une rubrique
      - Par interval de date (Ex : les 7 derniers jours, les 30 derniers jours, l'année en cours ...)

   - Rechercher un article par un ou plusieurs mots clefs

   - Corriger un article, c'est à dire modifier le texte pour corriger les fautes d'orthographes, pas de modification de l'ordre des blocs (Hypothèse)

   - Ajouter des mots-clés à un article
   - Associer un article à une ou plusieurs rubriques et/ou sous rubriques
   - Valider un article pour publication
   - Publier un article préalablement validé
   - Rejeter un article (avec une justification)
   - Renvoyer un article au rédacteur pour révision (avec une justification)

   - Créer un mot clef
   - Créer une rubrique et des sous rubriques
   - Créer un commité éditorial



   #### Modérateur
   - Masquer un commentaire
   - Supprimer un commentaire
   - Mettre en exergue un commentaire
   - Bannir un lecteur temporairement (Hypothèse)
   - Bannir un lecteur définitivement (Hypothèse)

   #### Lecteur
   - Lire un article
   - Voir tous les articles d'une rubrique / sous-rubrique
   - Voir tous les articles mis à l'honneur
   - Accèder aux articles liés depuis un article lu 

   - Rechercher un ou plusieurs articles par mots clefs (Hypothèse)

   - Déposer un commentaire à propos d'un article
   - Éditer un commentaire
   - Supprimer un de **ses** commentaire
   
   - Noter un article (1 à 5 Étoiles )

   

## HYPOTHESES :

- H1 : Le statut d'un article est déterminé selon son dernier évènement
- H2 : Un article est mis à l'honneur jusqu'à une certaine date
- H3 : Seul un administrateur à le droit de créer des rôles et des permission
- H4 : Un visiteur sans compte ne peut pas écrire de commentaire
- H5 : Noter un article n'est pas obligatoire pour laisser un commentaire
- H6 : Le slug d'un article ne peut être modifié une fois créé
- H7 : Le pseudonyme d'un utilisateur est unique
- H8 : Un utilisateur demandant à intégrer un commité editorial doit être approuvé par un membre du comité
- H9 : Un commité éditorial est spécialiste d'un ou plusieurs rubriques , incluant leur sous-rubriques
- H10 : L'éditeur doit vérifier qu'un mot clef n'existe pas déjà dans la base avant d'en créer un nouveau (recherche automatique)
- H11 : L'identifiant d'une permission ne peut pas être modifié car il impacte l'architecture de l'application
    