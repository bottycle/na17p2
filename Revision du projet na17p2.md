# Révision du projet na17p2

Ce document intervient dans la dernière phase du projet de base de données au sein de l'UV AI23. Il a été rédigé dans le but d'expliquer ma démarche pour la dernière phase du projet. 

# Révision de la normalisation

Afin de protéger la base de données en établissant des contraites relationnelles, il a été nécessaire d'identifier l'ensemble des dépendances fonctionnelles (DF)  du modèle relationnel. 

Le fichier [MLD.md](http://mld.md) contient désormais la liste des dépendances fonctionnelles pour chacunes des relations.

**Quelques remarques :** 

- Les timestamps "created_at" sont des clés sont (dans la réalité) des clefs : pour un petit blogs, il est impossible que deux articles, ou deux utilisateurs soient créés à la même miliseconde. Pour un projet de plus grande ampleur, je ne me risquerai pas à définir une contrainte UNIQUE.

    Cependant, étant donné que l'insertion des données se fait massivement, en une seule fois dans cet exercice, il n'est pas possible de poser une contraine **UNIQUE**  sur les timestamp car ces derniers sont déjà instancié par la valeur courante du serveur par défaut. 

- Les relations contenants des clés artificielles **id** , ont forcément pour DF : id ⇒ A, avec A la totalité des attributs de la relation.
- Pour un Block : le couple (article, title) à été identifié comme clé malgré l'existence d'une clé artificielle. On pose donc la contrainte **UNIQUE**(article, title)
- Dans le MLD j'ai identifié les DF sous la forme K → (a,b,c) , pour obtenir les DFE il suffit de diviser les DF sous la forme K → a, K→ b, K→c

# Création de l'API Rest (Laravel 7)

Pour terminer ce projet, j'ai souhaité tester l'implémentation d'une API Rest, étant donné que c'est le travail qui m'attend cet été au sein de mon équipe de développement. 

J'ai donc appris à utiliser Laravel, ayant déjà un bon passé en PHP orienté objet. 

## Création des routes

Les routes constituent les points d'entrés d'une API. Elles permettent de lier une URL à une méthode au sein d'un controller. Le patron de conception REST défini la norme de nommage des urls suivante : 

```
GET articles => Retourne tous les articles de la bdd
GET articles/slug => Retourne l'article possedant ce slug
POST articles => Envoie un nouvel article en bdd
PUT articles/slug => Modifie l'article possèdant ce slug
DELETE articles/slug => Supprime l'article possedant ce slug
```

Ensuite, il suffit de relier ces méthodes HTTP aux méthodes du controller Laravel : 

```php
Route::get('articles', 'ArticleController@index');
Route::get('articles/{article}', 'ArticleController@get');
Route::post('articles', 'ArticleController@store');
Route::put('articles/{article}', 'ArticleController@update');
Route::delete('articles/{article}', 'ArticleController@destroy');
```

## Utilisation d'un ORM

Laravel utilise un ORM nommé Eloquent, afin de communiquer avec une base données. 

Cet ORM permet de récupérer des ensemble de données très simplement, en déclarant au préalable dans les model les relations entre les classes. 

**Exemple :** 

```php
class Article extends Model
{
    protected $table = 'articles.articles';
    protected $primaryKey = 'slug';
    protected $keyType = 'string';

    public function blocks()
    {
        return $this->hasMany(Block::class, 'article_slug');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'articles.article_category', 'article_slug', 'category_title');
    }
}
```

Ici, on indique juste à Laravel le nom de la table, le nom et le type de la clé primaire ainsi que les relations que le model Article peut avoir avec les autres models. Un hasMany() correspondant à une relation 1...* et un belongsToMany() correpondant à une relation *...*

## Ecriture des Controllers

Enfin les controllers permettent d'énoncer la logique de chacune des méthodes : création , mis à jour, suppression, etc. 

Avant d'inserer une donnée en BDD, on peut ajouter une vérification côté serveur pour ajouter un niveau de sécurité supplémentaire. 

**Exemple : Insertion d'un nouvel article**

```php
public function store(Request $request)
{
        Article::create($request->validate([
            'slug' => 'required',
            'title' => 'required|min:3|max:255',
            'showcase_url' => 'required'
        ]));

        return ["code" => 201, "response" => "Article created with success thanks to this AI23 Laravel API!"];
}
```

## Test de l'API avec Postman

Postman est un outils permettant de tester une API en réalisant des appels HTTP avec les métohdes POST, GET, PUT, DELETE et bien d'autres que nous n'étudierons pas ici. L'outils permet de passer des paramètres dans les reqiuêtes et d'afficher la réponse de l'API sous format JSON. 

Par exemple :

- un appel à l'url :  [a](http://monserveur.fr/articles)pi-na17p2.net/articles en GET renvoie tous les articles , avec leurs blocks respectifs, les évènements liés, les utilisateurs liés à ces évènements, etc. (Voir annexes)
- un appel à l'url : [api-na17p2.net/articles/](http://api-na17p2.net/articles/) en POST , avec les paramètres title, slug et showcase_url permet de créer un nouvel article dans la bdd. (Voir annexes)

    ![Revision%20du%20projet%20na17p2/Capture_decran_2020-06-15_a_17.48.45.png](Revision%20du%20projet%20na17p2/Capture_decran_2020-06-15_a_17.48.45.png)

## Tester par vous-même

Si jamais vous voulez tester l'API par vous même : il vous faut un serveur php , ou si vous avez installé php artisan : 

```json
cd ./api
php artisan serve
```

## Conclusion

La construction de cette api m'a permis de découvrir et d'appronfondir de nouvelles connaissances techniques : Laravel, ORM (Eloquent), JSON, API rest ... Une dernière étape qui n'a pas été abordée dans ce projet : la sécurisation de l'API, qui sort du champs de connaissance de l'UV. 

Ce fut une très grande satisfaction de voir l'API renvoyer des fichiers JSON contenant de nombreuses informations sur un articles. Il ne manquerai pas grand chose pour développer l'application sur mobile... 

# Annexes

### Page d'accueil pour tester l'API :

![Revision%20du%20projet%20na17p2/Capture_decran_2020-06-15_a_18.00.01.png](Revision%20du%20projet%20na17p2/Capture_decran_2020-06-15_a_18.00.01.png)

### Appel GET à users/michel.piccoli@gmail.com

```json
{
    "email": "michel.piccoli@gmail.com",
    "firstname": "Michel",
    "lastname": "Piccoli",
    "nickname": "Picolo",
    "mini_bio": null,
    "avatar": "https://robohash.org/dictaliberoaccusamus.jpg?size=50x50&set=set1",
    "role": "publisher",
    "created_at": "2018-11-03T22:14:32.000000Z",
    "committees": [
        {
            "name": "TopGear",
            "description": "We are in charge of every motorsport articles",
            "created_at": "2020-06-11T22:16:32.649628Z",
            "created_by": "rtillman1@ftc.gov",
            "pivot": {
                "user_email": "michel.piccoli@gmail.com",
                "committee_name": "TopGear"
            }
        }
    ]
}
```

### Appel à articles/how-to-validate-nf17

```json
{
    "article": {
        "slug": "how-to-validate-nf17",
        "title": "How to validate NF17",
        "showcase_url": "https://robohash.org/saepeeiusveritatis.bmp?size=50x50&set=set1",
        "honnored_at": null,
        "honnored_until": null,
        "honnored_by": null,
        "blocks": [
            {
                "id": 1,
                "article_slug": "how-to-validate-nf17",
                "title": "One of the most interesting UVs this semester",
                "content": "Sed ante. Vivamus tortor. Duis mattis egestas metus.",
                "image_url": null,
                "pos": 1
            },
            {
                "id": 2,
                "article_slug": "how-to-validate-nf17",
                "title": "I spent so much time working on it !",
                "content": "About 10 hours since the launch of the projet one weak ago",
                "image_url": null,
                "pos": 2
            }
        ],
        "categories": [
            {
                "title": "Database",
                "created_at": "2020-06-11T22:16:32.649628Z",
                "created_by": "jean.laporte@utc.fr",
                "parent_category": null,
                "committee": "Uvs",
                "pivot": {
                    "article_slug": "how-to-validate-nf17",
                    "category_title": "Database"
                }
            },
            {
                "title": "Postgre SQL",
                "created_at": "2020-06-11T22:16:32.649628Z",
                "created_by": "jean.laporte@utc.fr",
                "parent_category": "SQL",
                "committee": "Uvs",
                "pivot": {
                    "article_slug": "how-to-validate-nf17",
                    "category_title": "Postgre SQL"
                }
            }
        ]
    }
}
```

### Appel à DELETE articles/hi-api

![Revision%20du%20projet%20na17p27/Capture_decran_2020-06-15_a_17.56.02.png](Revision%20du%20projet%20na17p2/Capture_decran_2020-06-15_a_17.56.02.png)