# Relations : 

## Package Users
1. Roles(#title: varchar) avec :
    - taille(title) > 2

2. Permissions(#identifier: varchar, description: varchar) avec : 
    - taille(identifier) > 4
    - description not null

    *DF* :
    - identifier => description

3. Role_permission(#role=>Role, #permission=>Permissions)

4. Users(#email: varchar, password: varchar, firstname: varchar, lastname: varchar, nickname: varchar, mini_bio: text, avatar: varchar, role=>Role, created_at: date_heure)
avec : 
    - password, nickname, created_at, role not null
    - created_at date et heure courante par défaut
    - avatar commençant par 'http' car url

    *DF* : 
    - email => (password, firstname, lastname, nickname, mini-bio, avatar, role)
    - nickname => (email, password, firstname, lastname, mini_bio, avatar, role) (clé candidate)
    - created_at => (password, firstname, lastname, nickname, mini-bio, avatar, role) (clé candidate)

## Package Committees
5. Editorial_committees(#name: varchar, description: text, created_at: date_heure, created_by => Users) avec:
    - description, created_at, created_by not null
    - created_at date et heure courante par défaut

    *DF* : 
    - name => (description, created_at, created_by)

6. Committee_user(#user => Users, #committee => Editorial_Committees, status: Status, requested_at: date_heure, decided_at: date_heure) avec : 
    - status, requested_at not null
    - Status est un type ENUM : {'pending', 'approved', 'refused'}
    - requested_at date et heure courante par défaut
    - validated_at > requested_at

    *DF* : 
    - (user, committee) => (status, requested_at, decided_at)

## Package Articles

7. Articles(#slug: varchar, title: varchar, showcase_url: varchar, honnored_by => Users, honnored_at => date_heure, honnored_until => date_heure) avec : 
    - title, showcase_url not null, 
    - showcase_url commençant par 'http' car url
    - honnored_until > honnored_at

    *DF* : 
    - slug => (title, showcase_url, honnored_by, honnored_at, honnored_until)

8. Blocks(#id: int, article => Articles, title: varchar, content: Text, image_url: VARCHAR, pos: int) avec :
    - id auto increment
    - article, title, pos not null
    - SI content not null => image_url null SINON image_url not null
    - image_url commençant par 'http' car url
    - pos entier strictement positif

    *DF* : 
    - id => (article, title, content, image_url, pos)
    - (article, title) => (content, image_url, pos) (clé candidate)

9. Article_events(#id: int, article => Articles, user => Users, event: Article_EVT, justification: text, occured_at: date_heure) avec :
    - id auto increment,
    - article, user, event, occured_at not null,
    - Article_EVT est un type ENUM : {'created', 'edited', 'submited', 'validated', 'reported', 'rejected', 'published', 'deleted'}
    - occured_at date et heure courante par défaut

    *DF* : 
    - id => article, user, event, justification, occured_at
    - *Attention* : (user, event) n'est pas une clé candidate, un auteur peut soumettre deux fois le même article en cas de rejet préalable 

10. Comments(#id: int, article => Articles, title: varchar, content: text, rating: int, ref_comment => Comments) avec : 
    - id auto increment, 
    - article, title, content not null,
    - rating entier positif,
    - ref_comment pointe vers le commentaire parent

    *DF* : 
    - id => article, title, content, rating, ref_comment
    - *Attention* : (article, title) n'est pas une clé candidate, deux lecteurs peuvent écrire le titre de commenatire "Super !" sur un même article


11. Comment_events(#id: int, comment => Comments, user => Users, event: Comment_EVT, occured_at: date_heure) avec : 
    - id auto increment, 
    - comment, user, event, occured_at not null
    - Comment_EVT est un type ENUM : {'created', 'highlighted', 'hidden', 'deleted'}
    - occured_at date et heure courante par défaut

    *DF* : 
    - id => comment, user, event, occured_at


11. Keywords(#title: varchar, created_at: date_heure, created_by => Users) avec : 
    - created_at, created_by not null
    - created_at date et heure courante par défaut

    *DF* : 
    - title => created_at, created_by
    - (created_at, created_by) => title (clé candidate) (non implémentée en SQL car l'insertion avec TIME DEFAULT casse la contrainte car insertions massive )

12. Article_keyword(#article => Articles, #keyword => Keywords)


13. Categories(#title: varchar, created_at: date_heure, created_by => Users, parent_category => Categories, committee => Editorial_committees) avec :
    - created_at, created_by, committee not null,
    - created_at date et heure courante par défaut

    *DF* : 
    - title => (created_at, created_by, parent_category, committee)
    - (created_at, created_by) => (title, parent_category, committee) (clé candidate) (non implémentée en SQL car l'insertion avec TIME DEFAULT casse la contrainte car insertions massive )


14. Article_category(#article => Articles, #category => Category)
