--- ****** CREATION DES VUES ****** ----

--- ** ARTICLES PUBLIES ** ---
-- Affiche tous les articles publiés avec : 
-- - Leur auteur 
-- - Un appercu du premier bloc de texte (Les 50 premiers caractères + '...')
-- - Leur image de "couverture"
-- - Leur date de publication
-- N'affiche pas les articles publiés puis supprimés par la suite
-- Articles triés du plus récent au plus vieux
DROP VIEW IF EXISTS v_published_articles;
CREATE VIEW v_published_articles AS (
	SELECT A.title, A.slug, CONCAT(U.firstname, ' ', U.lastname) AS author, U.email as author_email, A.showcase_url AS overview_image, CONCAT(SUBSTRING(B.content from 1 for 50), '...') AS overview, E.occured_at AS published_at
	FROM Articles.articles AS A
	INNER JOIN Articles.article_events AS E ON A.slug = E.article_slug
	INNER JOIN Articles.blocks AS B ON B.article_slug = A.slug
	INNER JOIN Users.users AS U ON E.user_email = U.email
	WHERE E.event = 'published' 
		AND A.slug NOT IN (SELECT article_slug FROM Articles.article_events WHERE event = 'deleted') 
		AND B.pos = 1
	ORDER BY occured_at
);

-- Affichage global
SELECT * FROM v_published_articles;

-- Tous les articles écris par Trista Abadam publiés depuis le 1er Janvier 2020
SELECT * FROM v_published_articles WHERE author_email = 'tabadam13@pen.io' AND published_at >= DATE('2020-01-01 00:00:00');



--- ** ARTICLES SUPPRIMÉS ** ---
-- Affiche tous les articles supprimés (c'est à dire anciennement publiés et désormais non répertoriés) avec :
-- - Le titre de l'article
-- - Son slug
-- - L'auteur 
-- - Qui a supprimé l'article
-- - Justification de suppression
-- - Quand l'article a été supprimé
DROP VIEW IF EXISTS v_deleted_articles;
CREATE VIEW v_deleted_articles AS (
    SELECT A.title, A.slug, CONCAT(U.firstname, ' ', U.lastname) AS author, U.email as author_email, e_delete.user_email AS deleted_by, e_delete.justification, e_delete.occured_at AS deleted_at
	FROM Articles.articles AS A
    INNER JOIN Articles.article_events AS e_create ON A.slug = e_create.article_slug
	INNER JOIN Articles.article_events AS e_delete ON A.slug = e_delete.article_slug
	INNER JOIN Users.users AS U ON e_create.user_email = U.email
	INNER JOIN Users.users AS U2 ON e_delete.user_email = U2.email
	WHERE e_create.event = 'created'
		AND e_delete.event = 'deleted'
	ORDER BY e_delete.occured_at
);

-- Pourquoi l'article sur le fromage vegan a-t-il été supprimé ? 
SELECT justification FROM v_deleted_articles WHERE slug = 'how-about-vegan-cheese';





--- ** ARTICLES MIS A L'HONNEURS ** ---
-- Affiche tous les articles mis à l'honneurs avec : 
-- - L'auteur 
-- - Un appercu du premier bloc de texte (Les 50 premiers caractères + '...')
-- - Leur image de "couverture"
-- - La date de publication
-- - Qui a mis l'article à l'honneur
-- - Jusque quelle date 
-- N'affiche pas les articles mis à l'honneurs puis supprimés par la suite
-- Articles triés du plus récent au plus vieux
DROP VIEW IF EXISTS v_honnored_articles;
CREATE VIEW v_honnored_articles AS (
    SELECT slug, A.title, CONCAT(U.firstname, ' ', U.lastname) AS author, CONCAT(SUBSTRING(B.content from 1 for 50), '...') AS overview, E.occured_at AS published_at, CONCAT(U2.firstname, ' ', U2.lastname) AS honnored_by, A.honnored_at, A.honnored_until
    FROM Articles.articles AS A
    INNER JOIN Articles.article_events AS E ON A.slug = E.article_slug
    INNER JOIN Users.users AS U ON E.user_email = U.email
	INNER JOIN Users.users AS U2 ON A.honnored_by = U2.email
	INNER JOIN Articles.blocks AS B ON B.article_slug = A.slug

	WHERE E.event = 'published' 
		AND A.slug NOT IN (SELECT article_slug FROM Articles.article_events WHERE event = 'deleted') 		
		AND honnored_at IS NOT NULL
		AND B.pos = 1
	ORDER BY occured_at
);

-- Tous les articles qui apparaissent dans la catégorie mis à l'honneur en ce moment
SELECT * FROM v_honnored_articles WHERE honnored_at < NOW() AND honnored_until > NOW();


--- ** LOG DES ARTICLES ** ---
-- Affiche l'historique de toutes les actions liés aux articles avec : 
-- - Le titre de l'article
-- - L'evenement
-- - Qui a effectué l'action
-- - Quand cette action a été effectuée
DROP VIEW IF EXISTS v_article_log;
CREATE VIEW v_article_log AS (
	SELECT A.title, A.slug,  event, CONCAT(U.firstname, ' ', U.lastname) AS who, U.email,  E.occured_at as date
	FROM Articles.article_events AS E
	INNER JOIN Users.users AS U ON E.user_email = U.email
	INNER JOIN Articles.articles AS A ON E.article_slug = A.slug
);

-- L'historique de tous les évènements de l'année calendaire courante
SELECT * FROM v_article_log WHERE EXTRACT(YEAR FROM date) = EXTRACT(YEAR FROM NOW());


--- ** LOG DES COMMENTAIRES ** ---
-- Affiche l'historique de toutes les actions liés aux commentaires avec : 
-- - Le titre de l'article
-- - Le titre du commentaire
-- - Le contenu du commentaire
-- - L'action sur le commentaire
-- - Qui a effectué l'action
-- - Quel est son role
-- - Quand cette action a été effectuée
DROP VIEW IF EXISTS v_comment_log;
CREATE VIEW v_comment_log AS (
	SELECT A.title AS article, A.slug, C.title as comment_title, C.content as comment_content, E.event, CONCAT(U.firstname, ' ', U.lastname) AS who, U.role, E.occured_at
	FROM Articles.comments AS C
	INNER JOIN Articles.comment_events AS E ON C.id = E.comment_id
	INNER JOIN Users.users AS U ON E.user_email = U.email
	INNER JOIN Articles.articles AS A ON C.article_slug = A.slug
	ORDER BY E.occured_at DESC
);


-- Tous les commentaires supprimés : 
SELECT * FROM v_comment_log WHERE event = 'deleted' ORDER BY occured_at DESC;

-- Tous les commentaires mis en exergues pour un article donné : 
SELECT * FROM v_comment_log WHERE event = 'highlighted' AND slug = 'how-to-validate-nf17';

-- Tous les commentaires laissés sur l'article des trous noirs
SELECT * FROM v_comment_log WHERE event = 'created' AND slug = 'inside-black-holes';

																	
																	
--- ** NOTES DES ARTICLES ** ---
-- Affiche la moyenne des notes des articles
-- On affiche aussi combien de notes ont été laissée pour se faire une idée de la validité de la moyenne
DROP VIEW IF EXISTS v_rating;																
CREATE VIEW v_rating AS (
	SELECT A.title, count(c.id) AS nb, sum(C.rating)/count(c.id) as moyenne
	FROM Articles.comments AS C
	INNER JOIN Articles.articles AS A ON C.article_slug = A.slug
	WHERE C.rating IS NOT NULL
	GROUP BY A.title
);


SELECT * FROM v_rating;





