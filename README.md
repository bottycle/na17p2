# 16. Le-Fil électronique

Projet "Le-Fil électronique" de l'UV AI23. 

L'objectif du projet est de réaliser un système informatique permettant la gestion d'un journal étudiant en ligne. Le projet permettra la création d'articles, leur catégorisation, leur recherche, la gestion du suivi éditorial, et l'apposition de commentaires.
# Acteurs
Le système comportera les acteurs suivants :
Administrateur  : en charge de la gestion du système (création de comptes...)
Rédacteur : en charge de la production des articles
Éditeur : en charge de la relecture, de la validation et de la publication des articles (les éditeurs sont regroupés au sein du comité éditorial)
Lecteur : utilisateur final du système, pouvant mettre des commentaires ou des notes sur les articles lus
Modérateur : en charge du suivi des commentaires et notes posées par les utilisateurs

# Besoins Auteur
Créer un nouvel article : Un article contient des blocs, ces blocs contiennent un titre et un texte ou bien un titre et une image (il n'y aura pas de gestion de mise en forme avancée : mise en gras, positionnement des images, etc.)
Voir ses articles en cours de rédaction, supprimer un article, récupérer un article supprimé
Soumettre un article au comité éditorial, l'article est alors dans un état soumis et il n'est plus modifiable.

# Besoins Éditeur
Voir l'ensemble des articles, par auteur, par date, par statut (voir ci-après)
Accéder aux articles et les corriger
Associer un statut à chaque article : en rédaction (les articles non soumis), soumis (statut initial donné par l'auteur), en relecture (relecture commencée par un éditeur), rejeté (associer une justification), à réviser (ajouter des préconisations), validé (il est publiable sur le site Web)
Ajouter des mots-clés aux articles (indexation)
Créer une arborescence de rubriques et sous-rubriques
Associer les articles à une ou plusieurs rubriques et/ou sous-rubriques
Sélectionner parmi les articles validés ceux qui sont publiés sur le site
Proposer une catégorie "à l'honneur"
Lier des articles entre eux (associer deux articles)

# Besoins Lecteurs
Visualiser les articles
Accéder aux articles par rubrique et/ou sous-rubrique
Effectuer une recherche par mots clés
Accéder à la catégorie à l'honneur
Accéder aux articles liés depuis un article lu
Associer un commentaire à un article (un commentaire est composé d'un titre et de texte)
Supprimer ses commentaires
Lire les commentaires (non masqués) des autres (voir ci-après)

# Besoins Modérateur
Masquer un commentaire
Supprimer un commentaire
Mettre en exergue un commentaire

# Historisation et traçage
On modélisera un système permettant de garder la trace de l'ensemble des articles et commentaires supprimés.
Par ailleurs les actions des auteurs, modérateurs et éditeurs doivent être enregistrées (on veut savoir qui a rédigé un article, qui l'a validé, qui a masqué un commentaire...).