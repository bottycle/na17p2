# Preuve de normalisation

## 1NF
Tous mes relations possèdent au moins une clef et la totalité de leur attributs sont atomiques. 

**Exemple :**  Un article possède plusieurs catégories. Au lieu de lister toutes les catégorie dans un enregistrement, je créer une relation Article_categorie, qui référence les catégorie liés à cet article. 


## BCNF
Toutes mes relations sont en BCNF car seules les DFE existantes sont de la forme K -> A avec K une clé, et A un attribut non clef de la relation

**Exemple :** Un article peut subir de nombreux événements par plusieurs utilisateurs différents.
Dans mon MLD , pour un Article_event on a article qui référence un article , user qui référence un utilisateur, et la clef est un ID car (article, user) n'est pas pertinente étant donné qu'un même article peut subir deux événements différents par le même utilisateur.

## Redondance
Etant donné que l'ensemble de mes tables sont en BCNF , on peut dire que la base données n'est pas redondante. 


# Important : 
Ne sachant pas trop comment justifier la normalisation, je vous invite à me poser des questions durant la soutenance afin que je puisse vous expliquer au mieux mes décompositions. 


